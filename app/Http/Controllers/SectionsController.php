<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Departments;
use App\Models\Sections;
use DB;
class SectionsController extends Controller
{


    public function __construct(){
        $this->middleware('auth');
    }
    public function departmentAjax(Request $request){

        $departmentName = $request->searchDepName;
        $sectionIds = $request->section_id;
        $sortBy = $request->sortby;
        $query = DB::table('departments');

        if(isset($departmentName)){
            $query->where('name','like','%'.$departmentName.'%');
        }
        if(isset($sectionIds)){
            $query->whereIn('section_id', (explode(',',$sectionIds)));
        }
        if(isset($sortBy)){
            if($sortBy=='alphabetical'){
                $query->orderBy('name');
            }else if($sortBy=='byNewest'){
                $query->orderBy('created_at','DESC');
            }
        }
        $total=$query->count();
        $departments = $query->paginate(6);
        return view('departments.template',['departments'=>$departments,'total'=>$total]);
    }

}
