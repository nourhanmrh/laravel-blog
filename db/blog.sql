-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Feb 07, 2022 at 06:41 PM
-- Server version: 5.7.26
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `blog`
--

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

DROP TABLE IF EXISTS `departments`;
CREATE TABLE IF NOT EXISTS `departments` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `section_id` int(10) UNSIGNED NOT NULL,
  `image_path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `departments_section_id_foreign` (`section_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `section_id`, `image_path`, `name`, `description`, `link`, `created_at`, `updated_at`) VALUES
(1, 3, '1644090462-andoid.png', 'android Team', 'Digital is our business area focused on the final customer and our approach to webs and apps consuming our core platform services including Android and iOS developments.', 'www.google.com', '2022-02-05 17:47:42', '2022-02-05 17:47:42'),
(2, 1, '1644090875-Php Team.jpg', 'Php Team', 'PHP is a difficult language to tame, and puts a greater than usual burden on the developer to ensure the application’s stability and performance.', 'www.google.com', '2022-02-05 17:54:35', '2022-02-05 17:54:35'),
(3, 1, '1644090921-.NET Team.png', '.NET Team', 'The . Net Team Leader must have an extensive knowledge on various programming language to achieve high performance level on web applications. ... Lead and mentor developers thru coaching and sharing of knowledge. Develop new functionality of software products.', 'www.google.com', '2022-02-05 17:55:21', '2022-02-05 17:55:21'),
(4, 2, '1644090971-Accounting Team.jpg', 'Accounting Team', 'As the business grows, one of the earliest functions many founders decide to outsource is financial management. It’s understandable — this can be a complex area that requires a lot of attention, and sometimes a level of expertise that is not realistic for the founder to develop themselves.', 'www.google.com', '2022-02-05 17:56:11', '2022-02-05 17:56:11'),
(5, 2, '1644091010-Finance Team.png', 'Finance Team', 'The finance department ensures the adequate and timely provision of funds for the business\'s operations. It is also the department\'s role to ensure the company pays its debtors and suppliers on time. The department also coordinates the monitoring of income and expenditures.', 'www.google.com', '2022-02-05 17:56:50', '2022-02-05 17:56:50'),
(6, 1, '1644091109-Java Team.jpg', 'Java Team', 'The Java server side team have a lot to offer says Jim, because they are a “highly experienced and versatile team that offer a real opportunity to work on some ...', 'www.google.com', '2022-02-05 17:58:29', '2022-02-05 17:58:29'),
(7, 1, '1644091156-Database Management.jpg', 'Database Management', 'Data management is the process of ingesting, storing, organizing and maintaining the data created and collected by an organization.', 'www.google.com', '2022-02-05 17:59:16', '2022-02-05 17:59:16'),
(8, 3, '1644090462-ios.png', 'ios Team', 'Digital is our business area focused on the final customer and our approach to webs and apps consuming our core platform services including Android and iOS developments.', 'www.google.com', '2022-02-06 08:21:00', NULL),
(9, 2, '1644091010-Finance Team.png', 'Compliance Team', 'A compliance officer is an individual who ensures that a company complies with its outside regulatory and legal requirements as well as internal policies ', 'wwww.google.com', '2022-02-01 22:00:00', NULL),
(10, 2, '1644091010-Finance Team.png', 'Auditing Team', 'An audit team is an independent function in an organization that helps management achieve its objectives by evaluating the risk and control environment.', 'wwww.google.com', NULL, '2022-02-05 22:00:00'),
(11, 1, '1644257156-Frontend Team.jpg', 'Frontend Team', 'A front-end developer is typically only one player on a team that designs and develops web sites, web applications, or native applications', 'www.google.com', '2022-02-07 16:05:56', '2022-02-07 16:05:56'),
(12, 3, '1644257261-Flutter.png', 'Flutter', 'A diverse network of Flutter developers. Meetups. Find a meetup group near you and connect with other Flutter developers', 'www.google.com', '2022-02-07 16:07:41', '2022-02-07 16:07:41'),
(13, 1, '1644257359-HTML Team.jpg', 'HTML Team', 'Flutter transforms the entire app development process. Build, test, and deploy beautiful mobile, web, desktop, and embedded apps from a single codebase.', 'www.google.com', '2022-02-07 16:09:19', '2022-02-07 16:09:19');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2022_02_05_192011_create_departments_table', 1),
(6, '2022_02_05_192913_create_sections_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

DROP TABLE IF EXISTS `personal_access_tokens`;
CREATE TABLE IF NOT EXISTS `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sections`
--

DROP TABLE IF EXISTS `sections`;
CREATE TABLE IF NOT EXISTS `sections` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sections`
--

INSERT INTO `sections` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Web', '2022-02-05 06:21:00', NULL),
(2, 'Finance', '2022-02-03 22:00:30', NULL),
(3, 'Mobile', '2022-02-03 22:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'nourhan mroueh', 'nourhanmroueh@gmail.com', NULL, '$2y$10$0UAI5Kon8OobcRdLGHVmbOMwOxeDsMqy1b0etpMDKMhnybwqwDmea', NULL, '2022-02-05 17:39:38', '2022-02-05 17:39:38');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
