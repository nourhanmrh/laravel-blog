<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\DepartmentsController;
use App\Http\Controllers\SectionsController;

Auth::routes();

Route::group(['middleware' => 'auth'], function(){

    Route::get('/get-departments-ajax',[SectionsController::class, 'departmentAjax']);

    Route::resource('/',DepartmentsController::class);
});




