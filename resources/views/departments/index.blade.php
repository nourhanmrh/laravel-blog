@extends('layouts.app')
@section('content')
    <div class="container-fluid blog">
        <div class="row">

        <div class=" col-lg-3 col-md-4 col-12">
            <div class="search-input">
                <input type="text" id="search" class="searchDep" placeholder="Search By Department">
                <button type="button" class="btn btn-primary btn-clear" value="submit">X Clear</button>
            </div>
            <div class="sort margin-top margin-bottom">
                <h5  class="leftTitle">Sort By:</h5>
                <label class="radio">  <input type="radio" name="name" class="sortBy"  value="alphabtical">Alphabetical Order </label>&nbsp;
               <label class="radio"> <input type="radio" name="name" class="sortBy" value="byNewest"> New to old </label>&nbsp;
            </div>
            <div class="row">
                <h5  class="leftTitle">Filter By Sections</h5>
                @foreach($sections as  $r)
                    <div class=" col-md-12 col-4">
                        <input class="sectionCheck" type="checkbox" value="{{$r->id }}" id="25off">
                        <label class="labelCheck">{{ ucfirst($r->name) }}</label> </div>
                @endforeach
            </div>
            <button type="button" class="btn btn-primary btn-filter" value="submit">Filter</button>
            <button type="button" class="btn btn-primary btn-reset" value="submit">Reset Filter</button>

        </div>
        <div class="col-lg-9 col-md-8 col-12 ajax-class">
            <div id="ajax_result">

            </div>

        </div>
        </div>

    </div>
@endsection
