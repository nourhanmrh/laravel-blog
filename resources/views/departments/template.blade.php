<div class="row">
  <h5 >Result: {{$total}} </h5>
</div>
<hr>
<div class="row">
    @foreach($departments as $dep)

        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 card">
            <div class="text-center margin-bottom margin-top">
                <img src="{{ asset('/images/'.$dep->image_path) }}" class="img-fluid width-image" >
            </div>
            <div class="title" >{{ucfirst($dep->name)}}</div>
            <div class="description">{{$dep->description}}</div>
            <div class="navigate margin-bottom"><a href="//{{$dep->link}}" target="_blank">Read More</a></div>
        </div>
    @endforeach
    <div class="col-12">
        {!! $departments->links() !!}

    </div>

</div>
