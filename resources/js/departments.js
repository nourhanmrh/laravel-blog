

$(document).ready(function(){
    ajax_call();

    function ajax_call(page){
    var search = $('#search').val();
    var sortby = $(".sortBy:checked").val();
    var sectionsIds = [];
    $('.sectionCheck:checked').each(function(){
    sectionsIds.push($(this).val());
        });
    var data = 'searchDepName=' + search + '&section_id=' + sectionsIds + '&sortby=' +sortby ;

    $.ajax({
        url: "/get-departments-ajax?page="+page, type: "get",
    data: data,
    success:function(data){
    if(data != ""){

    $("#ajax_result").html(data);
    } else {
    $("#ajax_result").html("<p>No data Avaliable</p>");
    }
      }
        });

    };
    //on key press enter search for the title
    $(".searchDep").keyup(function(e){
        if (e.key === 'Enter' || e.keyCode === 13) {
            ajax_call();
        }
    });
    // on btn filter press do the filter
    $('.btn-filter').on('click', function () {
        ajax_call();
    });
    //reset all filters
    $('.btn-reset').on('click', function () {
        $(".searchDep").val("");
        $('.sortBy').prop('checked', false);
        $('.sectionCheck').prop('checked', false);
         ajax_call();
     });
    //clear input and reset data
    $(".btn-clear").click(function() {
        $(".searchDep").val("");
        ajax_call();
    });

        $(document).on('click','.pagination a',function(event){
            event.preventDefault();
            $('li').removeClass('active');
            $(this).parent('li').addClass('active');
            var url = $(this).attr('href');
            var page = $(this).attr('href').split('page=')[1];
            ajax_call(page);
        });

});




