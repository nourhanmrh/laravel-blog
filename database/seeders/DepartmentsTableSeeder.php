<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB; // Import DB facade
use Illuminate\Support\Carbon; // (Optional) If you're using Carbon for date handling

class DepartmentsTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('departments')->insert([
            [
                'section_id' => 1,
                'image_path' => 'images/department1.jpg',
                'name' => 'Human Resources',
                'description' => 'Handles recruitment, training, and employee relations.',
                'link' => 'http://example.com/hr',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'section_id' => 2,
                'image_path' => 'images/department2.jpg',
                'name' => 'Marketing',
                'description' => 'Focuses on market research, advertising, and promotions.',
                'link' => 'http://example.com/marketing',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'section_id' => 3,
                'image_path' => 'images/department3.jpg',
                'name' => 'Finance',
                'description' => 'Responsible for budgeting, accounting, and financial planning.',
                'link' => 'http://example.com/finance',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            
        ]);
    }
}
