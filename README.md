
Overview
This Project is a very simple  blogging platform that allows users to manage and interact with various departments and their sections. It provides features such as search, filter, and sort functionalities to enhance the user experience.

Features
Departments and Sections: Organize blog content into departments and their respective sections.
Search: Easily search for content across departments and sections.
Filter: Apply filters to narrow down content based on specific criteria.
Sort: Sort content based on various attributes to find what you need quickly.

To set up the project locally, follow these steps:

1- git clone https://nourhanmrh@bitbucket.org/nourhanmrh/laravel-blog.git
2-cd laravel-blog
3- composer install
4-add .env
5-php artisan key:generate
6-php artisan migrate
7-php artisan db:seed
Optional-- Import Sample Database (Optional)
mysql -u yourusername -p yourdatabase < db/blogs.sql
8-php artisan serve

